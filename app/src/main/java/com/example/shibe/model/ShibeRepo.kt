package com.example.shibe.model

import android.content.Context
import android.util.Log
import com.example.shibe.model.local.ShibeDatabase
import com.example.shibe.model.local.entity.Shibe
import com.example.shibe.model.remote.ShibeService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val TAG = "ShibeRepo"

class ShibeRepo @Inject constructor(val shibeService:ShibeService,@ApplicationContext context: Context) {

//    private val shibeService = ShibeService.getInstance()
    val shibeDao = ShibeDatabase.getInstance(context).shibeDao()
    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()


        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}