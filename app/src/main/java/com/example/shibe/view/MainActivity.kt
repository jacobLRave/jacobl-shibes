package com.example.shibe.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.shibe.R
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp
//Marks an Android component class to be setup for injection
@AndroidEntryPoint
// set the content view of the main activity xml and than that triggers the nav_graph to direct the fragment.
class MainActivity : AppCompatActivity(R.layout.activity_main)