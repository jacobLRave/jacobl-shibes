package com.example.shibe.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibe.adapter.HomeAdapter
import com.example.shibe.databinding.FragmentHomeBinding
import com.example.shibe.model.ShibeRepo
import com.example.shibe.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel by viewModels<HomeViewModel>()
    // This Injection will search for the repo class and if no instance exist it will create a new one
    // in this case it creates one because the viewmodel has not been called yet
    @Inject lateinit var repo: ShibeRepo
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.pSpin.isVisible = state.isLoading
            binding.rvList.apply {
                binding.btnChangeLayoutGrid.setOnClickListener() {
                    layoutManager = GridLayoutManager(context, 3)
                    binding.btnChangeLayoutLinear.isVisible = true
                    binding.btnChangeLayoutStaggered.isVisible = true
                    binding.btnChangeLayoutGrid.isVisible = false
                    adapter = HomeAdapter(repo).apply {
                        addCategory(state.shibes)
                    }
                }
                binding.btnChangeLayoutLinear.setOnClickListener() {
                    layoutManager = LinearLayoutManager(context)
                    binding.btnChangeLayoutGrid.isVisible = true
                    binding.btnChangeLayoutStaggered.isVisible = true
                    binding.btnChangeLayoutLinear.isVisible = false
                    adapter = HomeAdapter(repo).apply {
                        addCategory(state.shibes)
                    }

                }
                binding.btnChangeLayoutStaggered.setOnClickListener() {
                    layoutManager = StaggeredGridLayoutManager(3, 1)
                    binding.btnChangeLayoutGrid.isVisible = true
                    binding.btnChangeLayoutLinear.isVisible = true
                    binding.btnChangeLayoutStaggered.isVisible = false
                    adapter = HomeAdapter(repo).apply {
                        addCategory(state.shibes)
                    }

                }

                adapter = HomeAdapter(repo).apply {
                    addCategory(state.shibes)
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}